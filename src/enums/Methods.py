from enum import Enum


class Methods(Enum):
    LTC_LNC = "ltc-lnc"
    LTN_LNN = "ltn-lnn"
